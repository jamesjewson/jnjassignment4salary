#include <iostream>
#include <conio.h>


struct Person {
	int ID;
	std::string fName;
	std::string lName;
	double payRate;
	double hours;

};




int main() 
{
	double grossPay = 0;
	double totalGrossPay = 0;
	int numEmployees = 0;

	std::cout << "How many employees are you entering? \n";
	std::cin >> numEmployees;

	Person *employees = new Person[numEmployees];


	//Enter Employee Data
	for (int i = 0; i < numEmployees; i++ )
	{
		std::cout << "Please enter the ID of employee " << (i + 1) << "\n";
		std::cin >> employees[i].ID;

		std::cout << "Please enter the first name of employee " << (i + 1) << "\n";
		std::cin >> employees[i].fName;

		std::cout << "Please enter the last name of employee " << (i + 1) << "\n";
		std::cin >> employees[i].lName;

		std::cout << "Please enter the pay rate for " << employees[i].fName << " " << employees[i].lName << "\n";
		std::cin >> employees[i].payRate;

		std::cout << "Please enter the total hours for " << employees[i].fName << " " << employees[i].lName << "\n";
		std::cin >> employees[i].hours;

	}

	// Do math and output
	for (int i = 0; i < numEmployees; i++)
	{
		grossPay = employees[i].hours * employees[i].payRate;
		totalGrossPay += grossPay;

		std::cout << "Gross pay for " << employees[i].fName << " " << employees[i].lName << " with ID " << employees[i].ID << " is $" << grossPay << "\n";


	}
	std::cout << "Total gross pay is $" << totalGrossPay;


	_getch();
	return 0;
}